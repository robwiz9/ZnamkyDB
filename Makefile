CC := gcc
CFLAGS := -std=c99 -Wall -Wextra -pedantic -g -fdiagnostics-color=always

PROJECT:= ZnamkyDB
SRCS := main.c cJSON.c
ALLFILES= main.c cJSON.c cJSON.h Martin.json gvid.h README.md Makefile ZnamkyDB_CBP.cbp ZnamkyDB_CBP.depend ZnamkyDB_CBP.layout

OBJS := $(SRCS:.c=.o)

TARGET := $(PROJECT).exe
ZIP_FILENAME := $(PROJECT).zip

.PHONY: ph

ph: clean all

clean:
	del $(OBJS) $(TARGET)

all: $(TARGET)

$(TARGET): $(OBJS)
	$(CC) $(CFLAGS) -o $@ $^

%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<
