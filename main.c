/*
 * Projekt: Ročníkový projekt = Databáze známek
 * Autor: Martin Zezula 4. F
 * Datum: 04/2024
 */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "cJSON.h"
#include "gvid.h"

// Výchozí cesta k databázi, lze specifikovat vlastní v programu
#define DBPATH "Martin.json"


// Zkracuju si názvy cJSON funkcí, protože jsou moc dlouhé
#define forEach cJSON_ArrayForEach
#define get cJSON_GetObjectItemCaseSensitive
#define isNumber cJSON_IsNumber
#define getNumber cJSON_GetNumberValue
#define isString cJSON_IsString
#define getString cJSON_GetStringValue
#define isBool cJSON_IsBool
#define isObject cJSON_IsObject
#define isArray cJSON_IsArray
#define has cJSON_HasObjectItem

// Výchozí délka pole se známkami, existuje v podstatě pouze pro dřívější spuštění realloc()
#define DEFAULT_MAX 1

// Maximální počet prvků v poli rychlé nabídky, už se mi nechtělo řešit dvourozměrné pole přes malloc()
#define MAX_PREDMETY 50
#define MAX_UCITELE 100
#define MAX_SKOLNIROKY 50
#define MAX_POLOLETI 10

// Maximální delka, kterou jsem ochotný vyhradit pro patřičný údaj přes malloc()
#define STRLEN_PREDMET 45 + 1
#define STRLEN_PREDMETZKR 6 + 1
#define STRLEN_UCITELZKR 6 + 1
#define STRLEN_UCITEL 35 + 1
#define STRLEN_POPIS 100 + 1
#define STRLEN_SKOLNIROK 15 + 1
#define STRLEN_POLOLETI 15 + 1

/**
 * @brief Struktura pro uchovani jedne znamky
 * @param ID jedinecny identifikator znamky, pokazde inkrementovany o 1
 * @param znamka ciselna hodnota znamky
 * @param vaha vaha znamky do promeru
 * @param popis okolnosti kolem znamky, napr. "test na radici algoritmy"
 * @param ucitel jmeno + prijmeni ucitele
 * @param ucitelZkr zkratka ucitele jako v rozvrhu. napr. "Dri"
 * @param predmet nazev predmetu, napr. "Biologie"
 * @param predmetZkr zkratka nazvu predmetu jako v rozvrhu, napr. "Bi"
 * @param skolniRok skolni rok jako string, napr. "2023/2024"
 * @param pololeti pololeti jako string, napr. "1. pololeti"
 * @param unixDate unix timestamp, ze ktereho se stavi objekt data
 * @param tm objekt, ktery obsahuje jednotlive rozepsane info o datu
*/
typedef struct {
  int ID;
  double znamka;
  double vaha;
  char* popis;
  char* ucitel;
  char* ucitelZkr;
  char* predmet;
  char* predmetZkr;
  char* skolniRok;
  char* pololeti;
  time_t unixDate;
  struct tm tm;
} ZNAMKA;

/**
 * @brief Struktura pro uchovani vsech informaci, ktere potrebuju predavat mezi funkcemi odkazem
 * @param running urcuje, zda ma bezet hlavni cyklus s menu v main()
 * @param json aktualne nacteny json soubor do strukture typu cJSON
 * @param nextID ID pro znamku, ktere pouziju jako dalsi, kdyz budu vkladat novou. Na rozdil od atributu 'n' se nezmensuje, kdyz odstranim znamku z pole.
 * @param znamky pole se znamkami, v programu se meni jen minimalne
 * @param n aktualni pocet prvku v poli znamek
 * @param n_max maximalni pocet znamek, ktery v poli existoval v jeden moment, vyuzivam pro velikost v realloc
 * @param ptrA pomocne pole s pointery na prvky v realnem poli znamek, toto pole se meni, kdyz znamky filtruju a radim
 * @param n_ptrA aktualni pocet znamek v pomocnem poli
 * @param n_ptrA_max maximalni pocet znamek, ktery v pomocnem poli existoval v jeden moment
 *
 * @param predmety pole pro uchovani rychle nabidky s nazvy predmetu
 * @param n_predmety pocet prvku v poli predmety
 *
 * @param predmetyZkr pole pro uchovani rychle nabidky se zkratkami predmetu
 * @param n_predmetyZkr pocet prvku v poli predmetyZkr
 *
 * @param ucitele pole pro uchovani rychle nabidky se jmeny ucitelu
 * @param n_ucitele pocet prvku v poli ucitele
 *
 * @param uciteleZkr pole pro uchovani rychle nabidky se zkratkami ucitelu
 * @param n_uciteleZkr pocet prvku v poli ucitele
 *
 * @param skolniRoky pole pro uchovani rychle nabidky se skolnimi roky
 * @param n_skolniRoky pocet prvku v poli skolniRoky
 *
 * @param pololeti pole pro uchovani rychle nabidky pololetimi
 * @param n_pololeti pocet prvku v poli pololeti
 *
 * @param prikaz prostor na ulozeni celeho stringu prikazu, ktery uzivatel zadal v menu
*/
typedef struct {
  int running;
  cJSON* json;

  int nextID;
  ZNAMKA* znamky;
  int n;
  int n_max;

  ZNAMKA** ptrA;
  int n_ptrA;
  int n_ptrA_max;

  char* predmety[MAX_PREDMETY];
  int n_predmety;

  char* predmetyZkr[MAX_PREDMETY];
  int n_predmetyZkr;

  char* ucitele[MAX_UCITELE];
  int n_ucitele;

  char* uciteleZkr[MAX_UCITELE];
  int n_uciteleZkr;

  char* skolniRoky[MAX_SKOLNIROKY];
  int n_skolniRoky;

  char* pololeti[MAX_POLOLETI];
  int n_pololeti;

  char prikaz[20 + 1];

} DB;

/**
 * Struktura pro prenos dat z cJSON objektu ruznych datovych typu do nekolika promennych
*/
typedef struct {
  void* key;
  const cJSON* src;
  void* dest;

  char** hints;
  int* n_hints;
  char* arg1;
  int n_size;
  int n_max;
} TRANSFER;

/**
 * Struktura pro vytvoreni/upravu nove znamky v poli
*/
typedef struct {
  char key[29 + 1];
  void* value;
  int type;
  int n_max;

  char** hints;
  int* n_hints;
} EDIT;

typedef int (*Takce)(DB* db);

const char LOGO[]="     ______                      _         ____________ \n    |___  /                     | |        |  _  \\ ___ \\\n       / / _ __   __ _ _ __ ___ | | ___   _| | | | |_/ /\n      / / | '_ \\ / _` | '_ ` _ \\| |/ / | | | | | | ___ \\\n    ./ /__| | | | (_| | | | | | |   <| |_| | |/ /| |_/ /\n    \\_____/_| |_|\\__,_|_| |_| |_|_|\\_\\\\__, |___/ \\____/ \n                                       __/ |            \n                                      |___/             \n";


/**
 *  @brief Funkce pro zjisteni, zda se string nachazi v poli ukazatelu na stringy
 *
 *  @param str hledany string
 *  @param arr pole pointeru na string
 *  @param n delka pole
 *
 *  @return -1 pokud se v poli prvek nenachazi, jinak index hledaneho stringu v poli
 **/
int strIn(char* str, char* arr[], int n) {
  for (int i = 0; i < n; i++) {
    if (strcmp(str, arr[i]) == 0) return i;
  }
  return -1;
}


/**
 *  @brief Funkce pro vyhledani znamky v poli znamek pomoci binarniho vyhledavani
 *
 *  @param znamky pole znameky
 *  @param n delka pole
 *  @param ID hledane ID prvku
 *
 *  @return -1 pokud se v poli prvek nenachazi, jinak index hledane znamky v poli
 **/
int BinarySearch(ZNAMKA* znamky, int n, int ID) {
  int l = 0;
  int p = n;
  int mid = (l + p) / 2;

  while (l < p && znamky[mid].ID != ID) {
    if (znamky[mid].ID < ID) {
      l = mid;
    } else if (znamky[mid].ID > ID) {
      p = mid;
    }

    mid = (l + p) / 2;
  }
  if (znamky[mid].ID == ID) return mid;

  return -1;
}

/**
 *  @brief Funkce pro vyhledani znamky v poli pointeru na znamky pomoci binarniho vyhledavani
 *
 *  @param ptrA pole pointeru na znamky
 *  @param n_ptrA delka pole
 *  @param ID hledane ID prvku
 *
 *  @return -1 pokud se v poli prvek nenachazi, jinak index hledane znamky v poli
 **/
int BinarySearch_ptrA(ZNAMKA* ptrA[], int n_ptrA, int ID) {
  int l = 0;
  int p = n_ptrA;
  int mid = (l + p) / 2;

  while (l < p && (*ptrA[mid]).ID != ID) {
    if ((*ptrA[mid]).ID < ID) {
      l = mid;
    } else if ((*ptrA[mid]).ID > ID) {
      p = mid;
    }

    mid = (l + p) / 2;
  }
  if ((*ptrA[mid]).ID == ID) return mid;

  return -1;
}

/**
 *  @brief Funkce pro ziskani poctu cislic v intu
 *
 *  @param number cislo, ze ktereho chci pocitat delku
 *
 *  @return pocet cislic
 **/
int count_digits(int number) {
  int count = 0;
  if (number == 0) {
    return 1;
  }
  while (number > 0) {
    number /= 10;
    count++;
  }
  return count;
}

/**
 *  @brief Funkce pro ziskani unix timestamp z ISO date stringu
 *
 *  @param str ISO date string
 *  @param timestamp ukazatel na timestamp promennou, kam se vlozi vysledek
 *  @param tm ukazatel na struct tm, kam se vlozi prectene datum
 *
 *  @return -1 pokud nastala chyba pri cteni nebo konverzi na UNIX
 **/
int getUnixFromISO(char* str, time_t* timestamp, struct tm* tm) {
  (*tm).tm_year = 0;
  (*tm).tm_mon = 0;
  (*tm).tm_mday = 0;
  (*tm).tm_hour = 0;
  (*tm).tm_min = 0;
  (*tm).tm_sec = 0;

  int e = sscanf(str, " %d-%d-%dT%d:%d:%d ", &(*tm).tm_year, &(*tm).tm_mon,
                 &(*tm).tm_mday, &(*tm).tm_hour, &(*tm).tm_min, &(*tm).tm_sec);
  if (e < 3) {
    (*tm).tm_year = 1970;
    (*tm).tm_mon = 1;
    (*tm).tm_mday = 1;
    (*tm).tm_hour = 0;
    (*tm).tm_min = 0;
    (*tm).tm_sec = 0;
  }

  (*tm).tm_year -= 1900;
  (*tm).tm_mon -= 1;
  (*tm).tm_isdst = -1;

  *timestamp = mktime(tm);

  // Overeni, protoze to tak proste funguje
  if (timestamp == (time_t*)-1) {
    fprintf(stderr, "Chyba konvertovani datumu na UNIX\n");
    return -1;
  }

  return 0;
}

/**
 *  @brief Funkce pro ziskani ISO date stringu
 *  PO SKONCENI VYUZIVANI JE NUTNE ZAVOLAT free()
 *
 *  @param tm struktura se zaznamy o datu
 *  @return char* s ISO date stringem
 **/
char* getISOFromtm(struct tm tm) {
  char* str = malloc((19 + 1) * sizeof(char));
  if (str == NULL) {
    return NULL;
  }

  char format[] = "%4d-%02d-%02dT%02d:%02d:%02d";

  tm.tm_year += 1900;
  tm.tm_mon += 1;

  sprintf(str, format, tm.tm_year, tm.tm_mon, tm.tm_mday, tm.tm_hour, tm.tm_min,
          tm.tm_sec);
  return str;
}

/**
 *  @brief Funkce pro cJSON struktury do konzole
 *  Funkce nemusi fungovat spravne pri vetsim objemu cJSON struktury navzdory spravnemu formatu
 *
 *  @param json cJSON objekt
 **/
void VypisRaw(const cJSON* json) {
  char* str = cJSON_Print((const cJSON*)json);  // neřvi
  if (str == NULL) {
    printf("Chyba pri vytvareni mista v pameti.");
    return;
  }
  printf("%s\n", str);
  free(str);
  return;
}

/**
 *  @brief Funkce pro vypis jedne znamky
 *
 *  @param z1 znamka1
 **/
void VypisZnamku(ZNAMKA z1, int prt_mode) {
  int len_i1 = floor(log(z1.ID + 1) / 10.0) * 10 + 1;
  const char* pad = "=========";

  printf("= | i%d | %*.*s %*s%*s =====", z1.ID, 9 - len_i1, 9 - len_i1, pad,
         2 + (int)strlen(z1.predmetZkr) / 2, z1.predmetZkr,
         2 - (int)strlen(z1.predmetZkr) / 2, ""); // Muzu pouzit (int) cast, jelikoz delka tohoto stringu nikdy nepresahne INT_MAX

  int LNW = 35;
  int len_s1 = strlen(z1.popis);

  int lns1 = ceil(len_s1 / (LNW * 1.0));

  if (prt_mode == 1) {
    printf("\n* Predmet:       %s", z1.predmet);
  }

  for (int i = 0; i < lns1; i++) {
    if (i == 0)
      printf("\n* Popis:         ");
    else
      printf("\t\t ");

    if (len_s1 > LNW * i) {
      printf("%-*.*s", LNW + 1, LNW, &(z1.popis[LNW * i]));
    } else
      printf("%*s", LNW + 1, "");
    printf("\n");
  }
  if (lns1 == 0) printf("\n");

  printf("* Znamka a vaha: %.2f | %.2f\n", z1.znamka, z1.vaha);

  printf("* Ucitel:        (%s) %s\n", z1.ucitelZkr, z1.ucitel);

  printf("* Datum:         %02d. %02d. %04d\n", z1.tm.tm_mday, z1.tm.tm_mon + 1,
         z1.tm.tm_year + 1900);

  if (prt_mode == 1) {
    printf("* Pololeti:      %s\n", z1.pololeti);
    printf("* Skolni rok:    %s\n", z1.skolniRok);
    printf("* Unix TS:       %lu\n", (unsigned long)z1.unixDate);
  }

  printf("==============================\n");
}

/**
 *  @brief Funkce pro vypis dvou znamek vedle sebe
 *  Setri misto v konzoli, vyuziva okno konzole efektivneji
 *
 *  @param z1 znamka1
 *  @param z2 znamka2
 **/
void VypisDveZnamky(ZNAMKA z1, ZNAMKA z2) {
  int len_i1 = floor(log(z1.ID + 1) / 10.0) * 10 + 1;
  int len_i2 = floor(log(z1.ID + 1) / 10.0) * 10 + 1;

  const char* pad = "=========";

  // Formatovaci peklicko; snazim se vyrovnat delku zkratky predmetu tim, ze prida urcity pocet znaku '='
  printf("= | i%d | %*.*s %*s%*s =====", z1.ID, 9 - len_i1, 9 - len_i1, pad,
         2 + (int)strlen(z1.predmetZkr) / 2, z1.predmetZkr,
         2 - (int)strlen(z1.predmetZkr) / 2, "");
  printf("\t\t\t\t\t= | i%d | %*.*s %*s%*s =====\n", z2.ID, 9 - len_i2,
         9 - len_i2, pad, 2 + (int)strlen(z2.predmetZkr) / 2, z2.predmetZkr,
         2 - (int)strlen(z2.predmetZkr) / 2, ""); // Muzu pouzit (int) cast, jelikoz delka tohoto stringu nikdy nepresahne INT_MAX

  int LNW = 35;
  int len_s1 = strlen(z1.popis);
  int len_s2 = strlen(z2.popis);

  // Maximalne 35 znaku popisu znamky na jeden radek, dale radek zalamuju.
  //Musim ale resit pro obe znamky, protoze jsou vypsane vedle sebe
  int lns1 = ceil(len_s1 / (LNW * 1.0));
  int lns2 = ceil(len_s2 / (LNW * 1.0));
  int lns = lns1 > lns2 ? lns1 : lns2;

  for (int i = 0; i < lns; i++) {
    if (len_s1 > LNW * i || len_s2 > LNW * i) {
      if (i == 0)
        printf("* Popis:         ");
      else
        printf("*\t\t ");

      if (len_s1 > LNW * i) {
        printf("%-*.*s", LNW + 1, LNW, &(z1.popis[LNW * i]));
      } else
        printf("%*s", LNW + 1, "");

      if (i == 0)
        printf("\t\t* Popis:         ");
      else
        printf("\t\t*\t\t ");

      if (len_s2 > LNW * i) {
        printf("%-*.*s", LNW + 1, LNW, &(z2.popis[LNW * i]));
      } else
        printf("%*s", LNW + 1, "");
    }
    printf("\n");
  }
  if (lns1 == 0) printf("\n");

  printf("* Znamka a vaha: %.2f | %.2f", z1.znamka, z1.vaha);
  printf("\t\t\t\t\t* Znamka a vaha: %.2f | %.2f\n", z2.znamka, z2.vaha);

  printf("* Ucitel:        (%s) %s ", z1.ucitelZkr, z1.ucitel);
  printf("\t\t\t* Ucitel:        (%s) %s \n", z2.ucitelZkr, z2.ucitel);

  printf("* Datum:         %02d. %02d. %04d", z1.tm.tm_mday, z1.tm.tm_mon + 1,
         z1.tm.tm_year + 1900);
  printf("\t\t\t\t\t* Datum:         %02d. %02d. %04d\n", z2.tm.tm_mday,
         z2.tm.tm_mon + 1, z2.tm.tm_year + 1900);

  printf("==============================\t\t\t\t\t");
  printf("==============================\n\n");
}

/**
 *  @brief Funkce pro vypis rychle nabidky castych hodnot
 *
 *  @param hints pole s pointery na caste hodnoty
 *  @param n_hints delka pole
 **/
void vypisRychlouNabidku(char* hints[], int n_hints) {
  if (n_hints > 0) {
    printf("Rychla nabidka hodnot (Napiste cislo pro vyber):\n");
    for (int i = 0; i < n_hints; i++) {
      printf("* %2d) %s\n", i + 1, hints[i]);
    }
    printf("\n");
  }
}

/**
 *  @brief Funkce pro porovnani dvou znamek ve stylu strcmp(), ale s vice moznostmi
 *
 *  @param a znamka1
 *  @param b znamka2
 *  @param cmpType atribut objektu znamky, podle ktereho se budou znamky v poli radit
 *
 *  @return <0 pokud znamka1 je 'mensi' nez znamka2, 0 pokud se znamky 'rovnaji', >0 pokud znamka1 je 'vetsi' nez znamka2
 **/
double compareZnamka(ZNAMKA a, ZNAMKA b, int cmpType) {
  // Prohodime promenne a, b pokud radime sestupne
  if (cmpType < 0) {
    ZNAMKA c = a;
    a = b;
    b = c;

    cmpType = abs(cmpType);
  }

  switch (cmpType) {
    case 1:
      return (a.unixDate - b.unixDate * 1.0);
    case 2:
      return (a.znamka - b.znamka * 1.0);
    case 3:
      return strcmp(a.predmet, b.predmet);
    case 4:
      return strcmp(a.ucitelZkr, b.ucitelZkr);
  }
  return 0;
}

/**
 *  @brief Callback funkce, se pokusi nacist json soubor v mem formatu a prida ho do databaze
 *
 *  @param db Predana databaze
 *  @return -2, pokud nactene datum v objekt se nepodarilo prevest na unix timestamp, -1 pokud nastala chyba pri malloc nebo cteni ze souboru, 0 pokud nacitani probehlo uspesne
 **/
int NactiSoubor(DB* db) {
  fflush(stdin);

  char DB_path[120 + 1] = DBPATH;
  printf("Napiste cestu k souboru (ENTER pro %s):\n>> ", DB_path);
  scanf("%120[^\n]", DB_path);

  fflush(stdin);

  FILE* f = fopen(DB_path, "r");
  if (f == NULL) {
    fprintf(stderr, "Chyba pri otevirani souboru.\n");
    return -1;
  }
  printf("\nNacitam Soubor...\n");

  fseek(f, 0L, SEEK_END);
  unsigned long fileSize = ftell(f);

  rewind(f);

  char* jsonString = malloc(fileSize + 1);
  if (jsonString == NULL) {
    fprintf(stderr, "Chyba pri vytvareni mista v pameti.\n");
    fclose(f);
    return -1;
  }

  size_t bytesRead = fread(jsonString, 1, fileSize, f);
  if (bytesRead > fileSize) {  // fileSize je v mych podminkach konstantne vyssi
                               // nez bytesRead o +6
    fprintf(stderr, "Chyba pri cteni ze souboru.\n");
    fclose(f);
    free(jsonString);
    return -1;
  }
  jsonString[bytesRead] = '\0';

  db->json = cJSON_ParseWithLength(jsonString, bytesRead);

  if (db->json == NULL) {
    const char* errorPtr = cJSON_GetErrorPtr();

    if (errorPtr != NULL) {
      fprintf(stderr, "Chyba v souboru před: %s\n", errorPtr);
      return -1;
    }
  }

  fclose(f);
  free(jsonString);

  const cJSON* years = NULL;
  const cJSON* year = NULL;

  years = get((const cJSON*)db->json, "years");
  int yearIdx = 0;

  /** === Struktura parsovaneho objektu ===
   *   {
   *     "name" : ...,
   *     ...
   *     "years": {
   *       "2023/2024": {
   *         "1. pololeti": [
   *           {
   *             "subject": ...,
   *             ...
   *             "grades":[
   *               {
   *                 "grade": 1,
   *                 "topic": "Ustni zkouseni",
   *                 ...
   *               }
   *             ]
   *           }
   *         ]
   *       }
   *     }
   *   }
   */

  int n_before = db->n;

  // Zahajeni iterace nad JSON strukturou
  forEach(year, years) {  // year = { "1. pololeti": ... }
    if (strIn(year->string, db->skolniRoky, db->n_skolniRoky) == -1 &&
        db->n_skolniRoky < MAX_SKOLNIROKY) {
      char* yearStr = malloc(STRLEN_SKOLNIROK * sizeof(char));
      if (yearStr == NULL) {
        fprintf(stderr, "Chyba pri vytvareni mista v pameti.");
        return -1;
      }
      strcpy(yearStr, year->string);
      db->skolniRoky[db->n_skolniRoky] = yearStr;
      db->n_skolniRoky++;
    }

    const cJSON* term = NULL;

    int termIdx = 0;

    forEach(term, year) {  // term = [ { "subject": ... }, { ... } ]

      if (strIn(term->string, db->pololeti, db->n_pololeti) == -1 &&
          db->n_pololeti < MAX_POLOLETI) {
        char* termStr = malloc(STRLEN_POLOLETI * sizeof(char));
        if (termStr == NULL) {
          fprintf(stderr, "Chyba pri vytvareni mista v pameti.");
          return -1;
        }
        strcpy(termStr, term->string);
        db->pololeti[db->n_pololeti] = termStr;
        db->n_pololeti++;
      }

      const cJSON* subject = NULL;

      int subjectIdx = 0;

      forEach(subject, term) {  // subject = { "subject": ... , "grades": [] }

        const cJSON* grade = NULL;
        const cJSON* grades = get(
            subject,
            "grades");  // grades = [ { "teacher": ... , "grade": 1 }, {...} ]

        const cJSON* js_subject = get(subject, "subject");
        char* subjectStr = getString(js_subject);

        if (subjectStr == NULL) {
          fprintf(stderr, "Chyba pri vytvareni mista v pameti.");
          return -1;
        }

        const cJSON* js_subject_short = get(subject, "subject_short");
        char* subjectShortStr = getString(js_subject_short);

        if (subjectShortStr == NULL) {
          fprintf(stderr, "Chyba pri vytvareni mista v pameti.");
          return -1;
        }

        int gradeIdx = 0;

        forEach(grade, grades) {  // grade = { "teacher": ... , "grade": 1 }

          // Pokud aktualni pocet prvku presahuje nadefinovane maximum,
          // potrebuju vytvorit misto pro mozny novy prvek.
          if (db->n_max >= DEFAULT_MAX) {
            db->znamky = (ZNAMKA*)realloc(db->znamky,
                                          ((db->n_max) + 1) * sizeof(ZNAMKA));

            if (db->znamky == NULL) {
              fprintf(stderr, "Chyba pri vytvareni mista v pameti.");
              return -1;
            }
          }

          // #region TRANSFER
          cJSON* js = NULL;

          db->znamky[db->n].ID = db->nextID;
          db->nextID = db->nextID + 1;

          db->znamky[db->n].popis = malloc(STRLEN_POPIS * sizeof(char));
          db->znamky[db->n].ucitel = malloc(STRLEN_UCITEL * sizeof(char));
          db->znamky[db->n].ucitelZkr = malloc(STRLEN_UCITELZKR * sizeof(char));
          db->znamky[db->n].predmet = malloc(STRLEN_PREDMET * sizeof(char));
          db->znamky[db->n].predmetZkr =
              malloc(STRLEN_PREDMETZKR * sizeof(char));
          db->znamky[db->n].pololeti = malloc(STRLEN_POLOLETI * sizeof(char));
          db->znamky[db->n].skolniRok = malloc(STRLEN_SKOLNIROK * sizeof(char));

          if (NULL == db->znamky[db->n].popis ||
              NULL == db->znamky[db->n].ucitel ||
              NULL == db->znamky[db->n].ucitelZkr ||
              NULL == db->znamky[db->n].predmet ||
              NULL == db->znamky[db->n].predmetZkr ||
              NULL == db->znamky[db->n].pololeti ||
              NULL == db->znamky[db->n].skolniRok) {
            fprintf(stderr, "Chyba pri vytvareni mista v pameti.");
            return -1;
          }

          /* === Struktura TRANSFER ===
            (char*) key ... hodnota na klici key /samotny klic kdyz je null;
            (cJSON*) src ... zdroj dat;
            (void*) dest ... vlozit do;
            (void*) hints ... databaze unikatnich str hodnot, ktere se casto opakuji;
            (int*) n_hints ... aktualni pocet techto hodnot; ptr proto, abych mohl inkrementovat
            (int) n_max ... Maximalni pocet castych hodnot, pro kazde pole jine
            (char*) arg1 ... dalsi string, co vyuziju

          */
          int count = 10;  // Pocet prenasenych hodnot
          TRANSFER tr[] = {
              [0] = {.key = "topic",
                     .src = grade,
                     .dest = db->znamky[db->n].popis,
                     .hints = NULL,
                     .n_hints = NULL,
                     .n_size = 0,
                     .n_max = 0,
                     .arg1 = NULL},
              [1] = {.key = "teacher",
                     .src = grade,
                     .dest = db->znamky[db->n].ucitel,
                     .hints = db->ucitele,
                     .n_hints = &(db->n_ucitele),
                     .n_size = STRLEN_UCITEL,
                     .n_max = MAX_UCITELE,
                     .arg1 = NULL},
              [2] = {.key = "teacher_short",
                     .src = grade,
                     .dest = db->znamky[db->n].ucitelZkr,
                     .hints = db->uciteleZkr,
                     .n_hints = &(db->n_uciteleZkr),
                     .n_size = STRLEN_UCITELZKR,
                     .n_max = MAX_UCITELE,
                     .arg1 = NULL},
              [3] = {.key = "date",
                     .src = grade,
                     .dest = &(db->znamky[db->n].unixDate),
                     .hints = NULL,
                     .n_hints = NULL,
                     .n_size = 0,
                     .n_max = 0,
                     .arg1 = NULL},
              [4] = {.key = "grade",
                     .src = grade,
                     .dest = &(db->znamky[db->n].znamka),
                     .hints = NULL,
                     .n_hints = NULL,
                     .n_size = 0,
                     .n_max = 0,
                     .arg1 = NULL},
              [5] = {.key = "weight",
                     .src = grade,
                     .dest = &(db->znamky[db->n].vaha),
                     .hints = NULL,
                     .n_hints = NULL,
                     .n_size = 0,
                     .n_max = 0,
                     .arg1 = NULL},
              [6] = {.key = "subject",
                     .src = subject,
                     .dest = db->znamky[db->n].predmet,
                     .hints = db->predmety,
                     .n_hints = &(db->n_predmety),
                     .n_size = STRLEN_PREDMET,
                     .n_max = MAX_PREDMETY,
                     .arg1 = subjectStr},
              [7] = {.key = "subject_short",
                     .src = subject,
                     .dest = db->znamky[db->n].predmetZkr,
                     .hints = db->predmetyZkr,
                     .n_hints = &(db->n_predmetyZkr),
                     .n_size = STRLEN_PREDMETZKR,
                     .n_max = MAX_PREDMETY,
                     .arg1 = subjectShortStr},
              [8] = {.key = NULL,
                     .src = term,
                     .dest = db->znamky[db->n].pololeti,
                     .hints = db->pololeti,
                     .n_hints = &(db->n_pololeti),
                     .n_size = STRLEN_POLOLETI,
                     .n_max = MAX_POLOLETI,
                     .arg1 = term->string},
              [9] = {.key = NULL,
                     .src = year,
                     .dest = db->znamky[db->n].skolniRok,
                     .hints = db->skolniRoky,
                     .n_hints = &(db->n_skolniRoky),
                     .n_size = STRLEN_SKOLNIROK,
                     .n_max = MAX_SKOLNIROKY,
                     .arg1 = year->string},
          };

          struct tm* tmPtr = &(db->znamky[db->n].tm);
          // #endregion TRANSFER

          // hromadne overuju, zda se klic nachazi v objektu a nactu jeho
          // hodnotu pouze tehdy kdyz ano, jinak ponecham NULL
          for (int i = 0; i < count; i++) {
            if ((char*)tr[i].key != NULL &&
                ! has(tr[i].src, (char*)tr[i].key))
              continue;

            // Ziskam data podle klice, na kterem je prave rada
            if ((char*)tr[i].key == NULL)
              js = (cJSON*)NULL;
            else
              js = get(tr[i].src, (char*)tr[i].key);

            // Datum je potreba parsovat ze stringu na unix int, ma tedy take
            // jine zpracovani
            if (tr[i].key != NULL && strcmp((char*)tr[i].key, "date") == 0) {
              char* strVal = getString(js);
              if (strVal == NULL) {
                fprintf(stderr, "Chyba pri vytvareni mista v pameti.");
                return -1;
              }
              if (getUnixFromISO(strVal, ((time_t*)tr[i].dest), tmPtr) == -1) {
                fprintf(stderr, "Chyba pri konverzi data do UNIX formatu\n.");
                return -2;
              }
              free(strVal);
            }

            else if (js == (cJSON*)NULL || isString(js)) {
              if (js == (cJSON*)NULL) {
                if (tr[i].dest == NULL) {
                  fprintf(stderr, "Chyba pri vytvareni mista v pameti.");
                  return -1;
                }
                strcpy((char*)tr[i].dest, tr[i].src->string);
              }

              else if (i >= 6) {
                char* strVal;
                switch (i) {
                  case 6:
                    strVal = subjectStr;
                    break;
                  case 7:
                    strVal = subjectShortStr;
                    break;
                  case 8:
                    strVal = term->string;
                    break;
                  case 9:
                    strVal = year->string;
                    break;
                }
                strcpy((char*)tr[i].dest, strVal);

              }

              else {
                char* strVal = getString(js);
                strcpy((char*)tr[i].dest, strVal);
                free(strVal);
              }

              if (tr[i].hints != NULL && tr[i].n_hints != NULL &&
                  *tr[i].n_hints < tr[i].n_max) {
                int idx =
                    strIn((char*)tr[i].dest, tr[i].hints, *tr[i].n_hints);
                if (idx == -1) {
                  tr[i].hints[*tr[i].n_hints] =
                      malloc(tr[i].n_size * sizeof(char));
                  if (tr[i].hints[*tr[i].n_hints] == NULL) {
                    fprintf(stderr, "Chyba pri vytvareni mista v pameti.");
                    return -1;
                  }
                  strcpy(tr[i].hints[*tr[i].n_hints], (char*)tr[i].dest);
                  *tr[i].n_hints = *tr[i].n_hints + 1;
                }
              }
            }

            // Pokud byla data typu cislo (double), vkladam standardne.
            else if (isNumber(js)) {
              double doubleVal = getNumber(js);
              *((double*)tr[i].dest) = doubleVal;
            }
          }

          db->n = db->n + 1;
          db->n_max = db->n;

          gradeIdx++;
        }

        free(subjectStr);
        free(subjectShortStr);
        subjectIdx++;
      }
      termIdx++;
    }
    yearIdx++;
  }
  db->n_ptrA = db->n;
  db->n_ptrA_max = db->n_max;

  db->ptrA =
      (ZNAMKA**)realloc(db->ptrA, (db->n_ptrA_max + 1) * sizeof(ZNAMKA*));
  if (*db->ptrA == NULL) {
    fprintf(stderr, "Chyba pri vytvareni mista v pameti\n.");
    return -1;
  }

  for (int i = 0; i < db->n; i++) (db->ptrA)[i] = &(db->znamky[i]);

  printf("V databazi je nyni %d prvku. (+%d)\n", db->n, db->n - n_before);
  return 0;
}

/**
 *  @brief Callback funkce, ktera vypise vsechny prvky v databazi
 *
 *  @param db Predana databaze
 *  @return 0
 **/
int VypisDB(DB* db) {
  printf("Pocet znamek: %d\n---------------\n", db->n);

  int i = 0;

  for (; i < (db->n - db->n % 2); i = i + 2) {
    VypisDveZnamky(db->znamky[i], (db->znamky)[i + 1]);
  }
  if (db->n_ptrA % 2 == 1) {
    VypisZnamku(db->znamky[i], 0);
  }

  return 0;
}

/**
 *  @brief Callback funkce, ktera vypise prvky v pomocnem poli
 *
 *  @param db Predana databaze
 *  @return 0
 **/
int VypisPom(DB* db) {
  printf("Pocet znamek v pomocnem poli: %d/%d\n---------------\n", db->n_ptrA,
        db->n);
  int i = 0;
  int i1 = i, i2 = i + 1;

  double prumer = 0.0;
  double soucet_vah = 0.0;

  for (; (i1 < db->n_ptrA - db->n_ptrA % 2) &&
         (i2 < db->n_ptrA - db->n_ptrA % 2);) {
    while (i1 == i2 || (db->ptrA)[i1] == NULL)
      i1++;  // Resim to takhle, abych mohl preskakovat NULL hodnoty v poli
    while (i2 == i1 || (db->ptrA)[i2] == NULL) i2++;

    if (!((i1 < db->n_ptrA - db->n_ptrA % 2) &&
            (i2 < db->n_ptrA - db->n_ptrA % 2)))
      break;

    prumer += 1.0 * (*(db->ptrA)[i1]).znamka * (*(db->ptrA)[i1]).vaha;
    prumer += 1.0 * (*(db->ptrA)[i2]).znamka * (*(db->ptrA)[i2]).vaha;

    soucet_vah += 1.0 * (*(db->ptrA)[i1]).vaha;
    soucet_vah += 1.0 * (*(db->ptrA)[i2]).vaha;

    VypisDveZnamky(*(db->ptrA)[i1], *(db->ptrA)[i2]);

    i1 += 2;
    i2 += 2;
  }

  if (db->n_ptrA % 2 == 1) {
    prumer += 1.0 * (*(db->ptrA)[db->n_ptrA - 1]).znamka * (*(db->ptrA)[db->n_ptrA - 1]).vaha;
    soucet_vah += 1.0 * (*(db->ptrA)[db->n_ptrA - 1]).vaha;

    VypisZnamku(*(db->ptrA)[db->n_ptrA - 1], 0);
  }

  if (db->n_ptrA > 0 && soucet_vah != 0){
    prumer /= soucet_vah * 1.0;
    printf("\nPrumer techto znamek je: %.2lf\n", prumer);
  }

  return 0;
}

/**
 *  @brief Funkce pro overeni, zda je pole serazeno podle typu razeni
 *
 *  @param pole pole ukazatelu na znamky
 *  @param l leva hranice mergesortu
 *  @param r prava hranice mergesortu
 *  @param cmpType atribut objektu znamky, podle ktereho se budou znamky v poli radit
 *
 *  @return 0 pokud pole neni serazene, 1 pokud pole je serazene
 **/
int checkSorted(ZNAMKA* pole[], int l, int r, int cmpType) {
  for (int i = l; i < r; i++) {
    if (compareZnamka(*pole[i], *pole[i + 1], cmpType) > 0.0) {
      return 0;
    }
  }
  return 1;
}

/**
 *  @brief Funkce pro provedeni slevaci faze nad pomocnym polem
 *
 *  @param pole pole ukazatelu na znamky
 *  @param l leva hranice mergesortu
 *  @param mid stredni index pole ... (l+r) / 2
 *  @param r prava hranice mergesortu
 *  @param cmpType atribut objektu znamky, podle ktereho se budou znamky v poli radit
 *
 *  @return -1 pokud neprobjehla slevaci faze razeni, 0 pokud razeni probjehlo uspesne, 1 pokud by melo rozdelovani na poloviny skoncit, 2 pokud je (usek) pole jiz serazeny
 **/
int MergePhase(ZNAMKA* pole[], int l, int r, int mid, int cmpType) {
  ZNAMKA* pompole[r - l + 1];
  memset(pompole, 0, sizeof(ZNAMKA*) * (r - l + 1));

  int l_mover = l;
  int r_mover = mid + 1;
  int pompole_mover = 0;

  while (l_mover <= mid && r_mover <= r) {
    if (compareZnamka(*pole[l_mover], *pole[r_mover], cmpType) <= 0) {
      pompole[pompole_mover++] = pole[l_mover++];
    } else {
      pompole[pompole_mover++] = pole[r_mover++];
    }
  }

  while (l_mover <= mid) pompole[pompole_mover++] = pole[l_mover++];
  while (r_mover <= r) pompole[pompole_mover++] = pole[r_mover++];

  memcpy(&(pole[l]), &(pompole[0]), sizeof(ZNAMKA*) * (r - l + 1));

  return 0;
}

/**
 *  @brief Funkce pro provedeni mergesortu nad pomocnym polem
 *
 *  @param pole pole ukazatelu na znamky
 *  @param l leva hranice mergesortu
 *  @param r prava hranice mergesortu
 *  @param cmpType atribut objektu znamky, podle ktereho se budou znamky v poli radit
 *
 *  @return -1 pokud neprobjehla slevaci faze razeni, 0 pokud razeni probjehlo uspesne, 1 pokud by melo rozdelovani na poloviny skoncit, 2 pokud je (usek) pole jiz serazeny
 **/
int MergeSort(ZNAMKA* pole[], int l, int r, int cmpType) {
  int mid = (l + r) / 2;
  if (!(l < r)) {
    return 1;
  }

  MergeSort(pole, l, mid, cmpType);
  MergeSort(pole, mid + 1, r, cmpType);

  if (checkSorted(pole, l, r, cmpType) == 1) {
    return 2;
  }

  if (MergePhase(pole, l, r, mid, cmpType) == 0) {
    return 0;
  }
  return -1;
}

/**
 *  @brief Callback funkce, ktera smaze znamku s urcitym ID, ktere specifikuje uzivatel.
 *
 *  Funkce posune znamky v hlavnim poli tak, aby se zde nenachazela mezera. Ovsem ID znamky se neuvolni pro dalsi pouziti.
 *  Funkce umoznuje zpracovat cely prikaz, napr "7 i28" aby primo z menu zahajila mazani, pokud takto byl poskytnut zpusob mazani.
 *  Funkce vyresetuje pomocne pole (bude nyni obsahovat vsechny prvky z hlavniho pole).
 *
 *  @param db Predana databaze
 *  @return 0 pokud uprava probehla
 *uspesne, 1 pokud bylo chybne zadano ID znamky
 **/
int SmazZnamku(DB* db) {
  int id = -1;
  int e = sscanf(db->prikaz, "7 i%d", &id);

  if (e < 1 || id == -1) {
    printf("\nZadejte ID znamky ve formatu \"i__\"\n>> ");
    scanf(" i%d", &id);
  }

  if (id < -1) {
    printf("Spatny format ID.\n\n");
    return 1;
  }
  if (id >= db->nextID) {
    printf("Prvek s timto ID neexistuje.\n\n");
    return 1;
  }
  if (db->n == 0) {
    printf("V poli nejsou zadne zaznamy.\nZkuste nacist soubor, nebo nejaky pridat.\n\n");
    return 1;
  }

  int idx = BinarySearch(db->znamky, db->n, id);
  int ptr_idx = BinarySearch_ptrA(db->ptrA, db->n_ptrA, id);
  if (idx == -1 || ptr_idx == -1) {
    printf("Prvek s timto ID neexistuje.\n\n");
    return 1;
  }
  printf("\nMazu znamku:\n");

  VypisZnamku(db->znamky[idx], 1);

  printf("\nOpravdu chcete smazat tuto znamku? (A/N)\n>> ");
  char answer;
  scanf(" %c", &answer);

  if (answer != 'A' && answer != 'a') {
    printf("Vracim se do menu..\n");
    return 1;
  }

  free(db->znamky[idx].pololeti);
  free(db->znamky[idx].popis);
  free(db->znamky[idx].predmet);
  free(db->znamky[idx].predmetZkr);
  free(db->znamky[idx].skolniRok);
  free(db->znamky[idx].ucitel);
  free(db->znamky[idx].ucitelZkr);

  memmove(&(db->znamky[idx]), &(db->znamky[idx + 1]),
          (db->n - idx - 1) * sizeof(ZNAMKA));
  db->n = db->n - 1;

  for (int i = 0; i < db->n; i++) (db->ptrA)[i] = &(db->znamky[i]);
  db->n_ptrA = db->n;

  printf("Znamka byla uspesne odstranena.\n");

  return 0;
}

/**
 *  @brief Callback funkce, ktera v databazi upravi jiz existujici znamku.
 *
 *  Funkce se zepta na tyto parametry v nasledujicim poradi:
 *  char[45] Predmet              ... Cely nazev predmetu
 *  char[6] Zkratka predmetu      ... Jako v rozvrhu, napr. "CJ"
 *  char[35] Ucitel               ... Cele jmeno ucitele
 *  char[6] Zkratka ucitele       ... Jako v rozvrhu, napr. "Dri"
 *  double Znamka                 ... Ciselna hodnota znamky
 *  double Vaha                   ... Vaha znamky, obvykle v rozmezi 0.25-1.0
 *  char[100] Popis               ... Popisek u znamky, napr. "Test radici
 *algoritmy" time_t Datum                  ... Datum znamky jako unix timestamp.
 *Ten se prevede do struktury typu struct tm, ktera bude obsahovat citelnou
 *podobu data.
 *
 *  Funkce bude pri vyplnovani textovych poli nabizet hodnoty, ktere se vyskytly
 *pri nacitani souboru. Umozni tak rychly vyber hodnot z nabidky napsanim cisla
 *odpovidajici hodnoty v nabidce.
 *
 * Funkce vyresetuje pomocne pole (bude nyni obsahovat vsechny prvky z hlavniho pole)
 *
 *  @param db Predana databaze
 *  @return -1, pokud se stala chyba behem upravy, 0 pokud uprava probehla
 *uspesne, 1 pokud bylo chybne zadano ID znamky
 **/
int UpravZnamku(DB* db) {
  int id = -1;
  int e = sscanf(db->prikaz, "5 i%d", &id);

  if (db->n == 0) {
    printf("V poli nejsou zadne zaznamy.\nZkuste nacist soubor, nebo nejaky pridat.\n\n");
    return 1;
  }

  if (e == 0 || id == -1) {
    printf("\nZadejte ID znamky ve formatu \"i__\"\n>> ");
    scanf(" i%d", &id);
  }

  if (id < -1) {
    printf("Spatny format ID.\n\n");
    return 1;
  }
  if (id >= db->nextID) {
    printf("Prvek s timto ID neexistuje.\n\n");
    return 1;
  }

  int idx = BinarySearch(db->znamky, db->n, id);
  if (idx == -1) {
    printf("Prvek s timto ID neexistuje.\n\n");
    return 1;
  }
  printf("\nUpravuji znamku:\n");

  VypisZnamku(db->znamky[idx], 1);

  int count = 8;
  EDIT hodnoty[] = {
      [0] = {.key = "Predmet",
             .value = db->znamky[idx].predmet,
             .type = 1,
             .n_max = STRLEN_PREDMET,
             .hints = db->predmety,
             .n_hints = &db->n_predmety},
      [1] = {.key = "Zkratka predmetu",
             .value = db->znamky[idx].predmetZkr,
             .type = 1,
             .n_max = STRLEN_PREDMETZKR,
             .hints = db->predmetyZkr,
             .n_hints = &db->n_predmetyZkr},
      [2] = {.key = "Ucitel",
             .value = db->znamky[idx].ucitel,
             .type = 1,
             .n_max = STRLEN_UCITEL,
             .hints = db->ucitele,
             .n_hints = &db->n_predmety},
      [3] = {.key = "Zkratka ucitele",
             .value = db->znamky[idx].ucitelZkr,
             .type = 1,
             .n_max = STRLEN_UCITELZKR,
             .hints = db->uciteleZkr,
             .n_hints = &db->n_predmety},
      [4] = {.key = "Znamka",
             .value = &db->znamky[idx].znamka,
             .type = 2,
             .n_max = 0,
             .hints = NULL,
             .n_hints = 0},
      [5] = {.key = "Vaha",
             .value = &db->znamky[idx].vaha,
             .type = 2,
             .n_max = 0,
             .hints = NULL,
             .n_hints = 0},
      [6] = {.key = "Popis znamky",
             .value = db->znamky[idx].popis,
             .type = 1,
             .n_max = STRLEN_POPIS,
             .hints = NULL,
             .n_hints = 0},
      [7] = {.key = "Datum",
             .value = &db->znamky[idx].unixDate,
             .type = 3,
             .n_max = 0,
             .hints = NULL,
             .n_hints = 0},
  };

  char* insert_str = malloc(1 * sizeof(char));
  size_t str_max = 1 * sizeof(char);

  if (insert_str == NULL) {
    fprintf(stderr, "Nepodarilo se vytvorit misto v pameti.\n");
    return -1;
  }
  char formatstr[10];
  printf("\n");

  for (int i = 0; i < count; i++) {
    fflush(stdin);
    printf("+============================+\n\n");
    printf("Upravuju pole: %-25s\n", hodnoty[i].key);

    if (hodnoty[i].type == 1) {
      strcpy(insert_str, "");

      if (hodnoty[i].n_max * sizeof(char) > str_max) {
        insert_str = realloc(insert_str, hodnoty[i].n_max * sizeof(char));
        if (insert_str == NULL) {
          fprintf(stderr, "Nepodarilo se vytvorit misto v pameti.\n");
          return -1;
        }
        str_max = hodnoty[i].n_max * sizeof(char);
      }

      printf("Aktualni hodnota: %-*s\n\n", hodnoty[i].n_max,
             (char*)hodnoty[i].value);
      sprintf(formatstr, "%%%d[^\n]", hodnoty[i].n_max);

      if (hodnoty[i].hints != NULL) {
        vypisRychlouNabidku(hodnoty[i].hints, *hodnoty[i].n_hints);
      }

      printf("Napiste novou hodnotu (ENTER pro ponechani)\n>> ");
      e = scanf(formatstr, insert_str);

      int num;
      if ((num = atoi(insert_str)) > 0 && hodnoty[i].n_hints != NULL &&
          num <= *hodnoty[i].n_hints) {
        printf("Zvoleno z nabidky: %s\n", hodnoty[i].hints[num - 1]);
        strcpy((char*)hodnoty[i].value, hodnoty[i].hints[num - 1]);
      } else if (e > 0) {
        strcpy((char*)hodnoty[i].value, insert_str);
      }

    } else if (hodnoty[i].type == 2) {
      printf("Aktualni hodnota: %.2lf\n\n", *(double*)hodnoty[i].value);
      printf("Napiste novou hodnotu (ENTER pro ponechani)\n>> ");

      char numberd[sizeof(double)];
      double d;
      fgets(numberd, sizeof(double), stdin);
      e = sscanf(numberd, "%lf", &d);
      if (e > 0) {
        *(double*)hodnoty[i].value = d;
      }
    }

    else if (hodnoty[i].type == 3) {
      printf("Aktualni hodnota: %d. %d. %d\n\n", db->znamky[idx].tm.tm_mday, db->znamky[idx].tm.tm_mon+1, db->znamky[idx].tm.tm_year+1900);
      printf("Napiste novou hodnotu v ISO formatu [YYYY]-[MM]-[DD]T[hh]:[mm]:[ss]\n(alespon rok, mesic a den; ENTER pro ponechani)\n>> ");

      char ISO[30+1];
      time_t t;
      e = scanf("%30[^\n]", ISO);
      if (e > 0){

        e = getUnixFromISO(ISO, &t, &(db->znamky[idx].tm));

        if (e > 0) {
          *(time_t*)hodnoty[i].value = t;

          if (db->znamky[idx].tm.tm_mon + 1 >= 9) {
            sprintf(db->znamky[idx].skolniRok, "%d/%d",
                    db->znamky[idx].tm.tm_year + 1900,
                    db->znamky[idx].tm.tm_year + 1900 + 1);
          } else {
            sprintf(db->znamky[idx].skolniRok, "%d/%d",
                    db->znamky[idx].tm.tm_year - 1 + 1900,
                    db->znamky[idx].tm.tm_year + 1900);
          }

          if (db->znamky[idx].tm.tm_mon + 1 >= 9 ||
              db->znamky[idx].tm.tm_mon + 1 < 2) {
            strcpy(db->znamky[idx].pololeti, "1. pololeti");
          } else {
            strcpy(db->znamky[idx].pololeti, "2. pololeti");
          }
        }

      }
    }
    if (e == 0) {
      printf("Hodnota nezmenena.\n");
    }

    printf("\n==============================\n");
  }

  free(insert_str);

  printf("\nZnamka byla upravena na:\n");
  VypisZnamku(db->znamky[idx], 1);
  printf("\n");
  return 0;
}

/**
 *  @brief Funkce, ktera porovnava urcitou hodnotu vuci objektu znamky, zda odpovida kriteriim.
 *
 *  1 ... Nazev predmetu
 *  2 ... Zkratka predmetu
 *  3 ... Cele jmeno ucitele
 *  4 ... Zkratka ucitele
 *  5 ... Ciselna hodnota znamky
 *  6 ... Vaha znamky
 *
 *  @param zn objekt znamky, ktery chci porovnavat
 *  @param fltType typ filtrovani, ktery se snazim aplikovat
 *  @param filter libovolna hodnota ve stringu (je pripadne prevedena na double) jako kriterium (filtr), ktery musi znamka splnovat
 *
 *  @return 0, pokud znamka neodpovida kriteriim, 1 pokud znamka odpovida kriteriim
 **/
int equals(ZNAMKA zn, int fltType, char* filter) {
  double znamka;
  double vaha;
  int e;
  switch (fltType) {
    case 1:
      return strcmp(zn.predmet, filter) == 0;
      break;

    case 2:
      return strcmp(zn.predmetZkr, filter) == 0;
      break;

    case 3:
      return strcmp(zn.ucitel, filter) == 0;
      break;

    case 4:
      return strcmp(zn.ucitelZkr, filter) == 0;
      break;

    case 5:

      e = sscanf(filter, "%lf", &znamka);
      if (e < 1) return -1;

      return znamka == zn.znamka;
      break;

    case 6:

      e = sscanf(filter, "%lf", &vaha);
      if (e < 1) return -1;

      return vaha == zn.vaha;
      break;

    default:
      break;
  }

  return -1;
}

/**
 *  @brief Callback funkce, ktera vyfiltruje prvky v pomocnem poli db->ptrA podle dale zadanych parametru.
 *
 *  Funkce umoznuje zpracovat cely prikaz, napr "8 1 Biologie" aby primo z menu zahajila filtrovani, pokud takto byl poskytnut zpusob filtrovani.
 *  Filtrovani podle xyz odpovida
 *  1 ... Nazev predmetu
 *  2 ... Zkratka predmetu
 *  3 ... Cele jmeno ucitele
 *  4 ... Zkratka ucitele
 *  5 ... Ciselna hodnota znamky
 *  6 ... Vaha znamky
 *
 *  Tim, ze funkce pracuje s pomocnym polem, je mozne spustit tuto funkci vicekrat pro dosahnuti filtrovani podle vice kriterii zaroven.
 *
 *  @param db Predana databaze
 *  @return -1, pokud se stala chyba behem filtrovani, 0 pokud filtrovani probehlo uspesne, 1 pokud uzivatel chybne zadal ID prvku
 **/
int FiltrujZnamky(DB* db) {
  int fltType = 0;
  char filter[50 + 1];

  if (db->n == 0) {
    printf("V poli nejsou zadne zaznamy.\nZkuste nacist soubor, nebo nejaky pridat.\n");
    return 1;
  }

  int e = sscanf(db->prikaz, "8 %d %51s", &fltType, filter);
  char** hints = NULL;
  int n_hints = 0;

  if (e < 1 || fltType < 1 || fltType > 6) {
    printf("---- Filtrovani znamek ----\n");
    printf("Podle ceho chcete filtrovat znamky?\n");
    printf(" 1 ... Podle predmetu\n");
    printf(" 2 ... Podle zkratky predmetu\n");
    printf(" 3 ... Podle ucitele\n");
    printf(" 4 ... Podle zkratky ucitele\n");
    printf(" 5 ... Podle znamky\n");
    printf(" 6 ... Podle vahy\n");
    printf("\nZadejte cislo\n>> ");
    e = scanf("%d", &fltType);
    fflush(stdin);





    if (e < 1 || fltType > 6) {
      printf("Nepatna hodnota filtrovani.\n");
      return -1;
    }
    printf("\n");
  }

  // Rychla nabidka hodnot pri filtrovani
  switch (fltType){
    case 1:
      hints = db->predmety;
      n_hints = db->n_predmety;
      break;
    case 2:
      hints = db->predmetyZkr;
      n_hints = db->n_predmetyZkr;
      break;
    case 3:
      hints = db->ucitele;
      n_hints = db->n_ucitele;
      break;
    case 4:
      hints = db->uciteleZkr;
      n_hints = db->n_uciteleZkr;
      break;
    default:
      break;
  }

  if (e < 2 || fltType < 1) {


    if (hints != NULL) {

      vypisRychlouNabidku(hints, n_hints);
    }
      printf("Napiste hodnotu filtru:\n>> ");
      e = scanf("%51s", filter);

    int num = 0;
    if ((num = atoi(filter)) > 0 && hints != NULL &&
        num <= n_hints) {
      printf("Zvoleno z nabidky: %s\n", hints[num - 1]);
      strcpy(filter, hints[num - 1]);

    }
  }
  fflush(stdin);
  int pos = 0;
  ZNAMKA** pompole = malloc(db->n_ptrA * sizeof(ZNAMKA*));
  if (pompole == NULL) {
    fprintf(stderr, "Nepodarilo se vytvorit misto v pameti.\n");
    return -1;
  }

  for (int i = 0; i < db->n_ptrA; i++) {
    if (equals(*db->ptrA[i], fltType, filter) == 1) {
      pompole[pos] = db->ptrA[i];
      pos++;
    }
  }

  for (int i = 0; i < pos; i++) {
    db->ptrA[i] = pompole[i];
  }
  db->n_ptrA = pos;

  printf("Filtru odpovida %d vysledku. Byly vlozeny do pomocneho pole.\n", pos);

  free(pompole);

  return 0;
}

/**
 *  @brief Callback funkce, ktera do databaze na konec pole vlozi novou znamku,
 *kterou zde take uzivatel vytvori.
 *
 *  Funkce se zepta na tyto parametry v nasledujicim poradi:
 *  char[45] Predmet              ... Cely nazev predmetu
 *  char[6] Zkratka predmetu      ... Jako v rozvrhu, napr. "CJ"
 *  char[35] Ucitel               ... Cele jmeno ucitele
 *  char[6] Zkratka ucitele       ... Jako v rozvrhu, napr. "Dri"
 *  double Znamka                 ... Ciselna hodnota znamky
 *  double Vaha                   ... Vaha znamky, obvykle v rozmezi 0.25-1.0
 *  char[100] Popis               ... Popisek u znamky, napr. "Test radici
 *algoritmy" time_t Datum                  ... Datum znamky jako unix timestamp.
 *Ten se prevede do struktury typu struct tm, ktera bude obsahovat citelnou
 *podobu data.
 *
 *  Funkce bude pri vyplnovani textovych poli nabizet hodnoty, ktere se vyskytly
 *pri nacitani souboru. Umozni tak rychly vyber hodnot z nabidky napsanim cisla
 *odpovidajici hodnoty v nabidce.
 *
 * Funkce vyresetuje pomocne pole (bude nyni obsahovat vsechny prvky z hlavniho pole)
 *
 *  @param db Predana databaze
 *  @return -1, pokud se stala chyba behem vkladani, 0 pokud vlozeni probehlo
 *uspesne
 **/
int VlozZnamku(DB* db) {
  // #region Pripadne zvetseni pole a malloc textovych poli
  if (db->n >= db->n_max) {
    db->znamky =
        (ZNAMKA*)realloc(db->znamky, ((db->n_max) + 1) * sizeof(ZNAMKA));
    db->ptrA = realloc(db->ptrA, sizeof(ZNAMKA*) * (db->n_ptrA_max + 1));
    db->n_max = db->n_max + 1;
    db->n_ptrA_max = db->n_ptrA_max + 1;
  }

  db->ptrA[db->n_ptrA] = &db->znamky[db->n];

  db->znamky[db->n].ID = db->nextID;
  db->nextID = db->nextID + 1;
  db->znamky[db->n].popis = malloc(STRLEN_POPIS * sizeof(char));
  db->znamky[db->n].ucitel = malloc(STRLEN_UCITEL * sizeof(char));
  db->znamky[db->n].ucitelZkr = malloc(STRLEN_UCITELZKR * sizeof(char));
  db->znamky[db->n].predmet = malloc(STRLEN_PREDMET * sizeof(char));
  db->znamky[db->n].predmetZkr = malloc(STRLEN_PREDMETZKR * sizeof(char));
  db->znamky[db->n].pololeti = malloc(STRLEN_POLOLETI * sizeof(char));
  db->znamky[db->n].skolniRok = malloc(STRLEN_SKOLNIROK * sizeof(char));

  if (NULL == db->znamky[db->n].popis || NULL == db->znamky[db->n].ucitel ||
      NULL == db->znamky[db->n].ucitelZkr ||
      NULL == db->znamky[db->n].predmet ||
      NULL == db->znamky[db->n].predmetZkr ||
      NULL == db->znamky[db->n].pololeti ||
      NULL == db->znamky[db->n].skolniRok) {
    fprintf(stderr, "Chyba pri vytvareni mista v pameti.");
    return -1;
  }
  // #endregion

  int count = 8;
  int e = 0;

  // Struktura s ukazateli na vkladana mista, typem nacitane hodnoty a pripadne napovedy (= rychla nabidka)
  EDIT hodnoty[] = {
      [0] = {.key = "Predmet",
             .value = db->znamky[db->n].predmet,
             .type = 1,
             .n_max = STRLEN_PREDMET,
             .hints = db->predmety,
             .n_hints = &db->n_predmety},
      [1] = {.key = "Zkratka predmetu",
             .value = db->znamky[db->n].predmetZkr,
             .type = 1,
             .n_max = STRLEN_PREDMETZKR,
             .hints = db->predmetyZkr,
             .n_hints = &db->n_predmetyZkr},
      [2] = {.key = "Ucitel",
             .value = db->znamky[db->n].ucitel,
             .type = 1,
             .n_max = STRLEN_UCITEL,
             .hints = db->ucitele,
             .n_hints = &db->n_predmety},
      [3] = {.key = "Zkratka ucitele",
             .value = db->znamky[db->n].ucitelZkr,
             .type = 1,
             .n_max = STRLEN_UCITELZKR,
             .hints = db->uciteleZkr,
             .n_hints = &db->n_predmety},
      [4] = {.key = "Znamka",
             .value = &db->znamky[db->n].znamka,
             .type = 2,
             .n_max = 0,
             .hints = NULL,
             .n_hints = NULL},
      [5] = {.key = "Vaha",
             .value = &db->znamky[db->n].vaha,
             .type = 2,
             .n_max = 0,
             .hints = NULL,
             .n_hints = NULL},
      [6] = {.key = "Popis znamky",
             .value = db->znamky[db->n].popis,
             .type = 1,
             .n_max = STRLEN_POPIS,
             .hints = NULL,
             .n_hints = NULL},
      [7] = {.key = "Datum",
             .value = &db->znamky[db->n].unixDate,
             .type = 3,
             .n_max = 0,
             .hints = NULL,
             .n_hints = NULL},
  };

  char formatstr[10];
  size_t size = 1 * sizeof(char);
  char* insert_str = malloc(size);

  if (insert_str == NULL) {
    fprintf(stderr, "Nepodarilo se vytvorit misto v pameti.\n");
    return -1;
  }

  printf("\n");

  for (int i = 0; i < count; i++) {
    fflush(stdin);
    printf("+============================+\n\n");
    printf("Upravuju pole: %-25s\n", hodnoty[i].key);

    // Kdyz nacitam string...
    if (hodnoty[i].type == 1) {
      strcpy(insert_str, "");

      if (size < (hodnoty[i].n_max) * sizeof(char)) {
        size = (hodnoty[i].n_max) * sizeof(char);
        insert_str = (char*)realloc(insert_str, size);
      }

      if (insert_str == NULL) {
        fprintf(stderr, "Nepodarilo se vytvorit misto v pameti.\n");
        return -1;
      }

      sprintf(formatstr, "%%%d[^\n]", hodnoty[i].n_max);

      if (hodnoty[i].hints != NULL) {
        vypisRychlouNabidku(hodnoty[i].hints, *hodnoty[i].n_hints);
      }

      printf("Napiste novou hodnotu (ENTER pro prazdne pole)\n>> ");
      e = scanf(formatstr, insert_str);

      int num;
      if ((num = atoi(insert_str)) > 0 && hodnoty[i].n_hints != NULL &&
          num <= *hodnoty[i].n_hints) {
        printf("Zvoleno z nabidky: %s\n", hodnoty[i].hints[num - 1]);
        strcpy((char*)hodnoty[i].value, hodnoty[i].hints[num - 1]);
      } else if (e > 0) {
        strcpy((char*)hodnoty[i].value, insert_str);

        if (hodnoty[i].n_hints != NULL && num <= *hodnoty[i].n_hints &&
            ! strIn(insert_str, hodnoty[i].hints, *hodnoty[i].n_hints)) {
          char* newstr = malloc(hodnoty[i].n_max * sizeof(char));
          if (newstr == NULL) {
            fprintf(stderr, "Nepodarilo se vytvorit misto v pameti.\n");
            return -1;
          }

          hodnoty[i].hints[*hodnoty[i].n_hints] = newstr;
          *hodnoty[i].n_hints = *hodnoty[i].n_hints + 1;
        }

      } else {
        strcpy((char*)hodnoty[i].value, "");
      }

    }
    // Kdyz nacitam double...
    else if (hodnoty[i].type == 2) {
      printf("Napiste hodnotu (ENTER pro 0)\n>> ");

      char numberd[sizeof(double)];
      double d;
      fgets(numberd, sizeof(double), stdin);
      e = sscanf(numberd, "%lf", &d);
      if (e > 0) {
        *(double*)hodnoty[i].value = d;
      } else {
        *(double*)hodnoty[i].value = 0;
      }

    }

    // Kdyz nacitam datum...
    else if (hodnoty[i].type == 3) {
      printf("\nNapiste novou hodnotu v ISO formatu [YYYY]-[MM]-[DD]T[hh]:[mm]:[ss]\n(alespon rok, mesic a den; ENTER pro 1. 1. 1970)\n>> ");

      char ISO[30+1];
      time_t t;
      e = scanf("%30[^\n]", ISO);
      if (e == 0){
        strcpy(ISO, "1970-01-01T00:00:00");
      }

      e = getUnixFromISO(ISO, &t, &(db->znamky[db->n].tm));

      *(time_t*)hodnoty[i].value = t;

      if (db->znamky[db->n].tm.tm_mon + 1 >= 9) {
        sprintf(db->znamky[db->n].skolniRok, "%d/%d",
                db->znamky[db->n].tm.tm_year + 1900,
                db->znamky[db->n].tm.tm_year + 1900 + 1);
      } else {
        sprintf(db->znamky[db->n].skolniRok, "%d/%d",
                db->znamky[db->n].tm.tm_year - 1 + 1900,
                db->znamky[db->n].tm.tm_year + 1900);
      }
      if (db->znamky[db->n].tm.tm_mon + 1 >= 9 ||
          db->znamky[db->n].tm.tm_mon + 1 < 2) {
        strcpy(db->znamky[db->n].pololeti, "1. pololeti");
      } else {
        strcpy(db->znamky[db->n].pololeti, "2. pololeti");
      }
    }
    printf("\n==============================\n");
  }
  free(insert_str);

  printf("\nVlozena znamka:\n");
  VypisZnamku(db->znamky[db->n], 1);
  printf("\n");
  db->n = db->n + 1;
  for (int i = 0; i < db->n; i++) (db->ptrA)[i] = &(db->znamky[i]);

  db->n_ptrA = db->n;

  return 0;
}
/**
 *  @brief Callback funkce, ktera seradi prvky v pomocnem poli db->ptrA podle
 *dale zadanych parametru. Razeni probiha metodou merge sort
 *
 *  Funkce umoznuje zpracovat cely prikaz, napr "6 +1" aby primo z menu zahajila
 *razeni, pokud takto byl poskytnut zpusob razeni. Razeni podle xyz odpovida 1
 *... Datum znamky 2 ... Ciselne hodnoty znamky 3 ... Nazev predmetu 4 ...
 *Zkratky ucitele Pokud pred cislo dodatecne zadate + / -, bude razeni probihat
 *vzestupne/sestupne ('+' neni nutne psat).
 *
 *  @param db Predana databaze
 *  @return -1, pokud se stala chyba behem razeni, 0 pokud razeni probehlo
 *uspesne, 1 pokud uzivatel chybne zadal ID prvku, 2 pokud pole je jiz serazene,
 **/
int SeradPole(DB* db) {

  if (db->n == 0) {
    printf("V poli nejsou zadne zaznamy.\nZkuste nacist soubor, nebo nejaky pridat.\n\n");
    return 1;
  }

  int cmpType = 0;
  int e = sscanf(db->prikaz, "6 %d", &cmpType);

  if (e == 0 || cmpType == 0) {
    printf("---- Razeni znamek ----\n");
    printf("Podle ceho chcete seradit znamky?\n");
    printf("vzestupne / sestupne\n");
    printf("  +1 / -1 ... Podle data znamky\n");
    printf("  +2 / -2 ... Podle znamky\n");
    printf("  +3 / -3 ... Podle nazvu predmetu\n");
    printf("  +4 / -4 ... Podle zkratky ucitele\n");
    printf("\nZadejte cislo\n>> ");

    scanf("%d", &cmpType);
    if (cmpType == 0 || cmpType > 4 || cmpType < -4) {
      printf("Zadane cislo neni mezi moznostmi. Vracim se do menu...\n");
      return 1;
    }
  }

  int i = 0;
  if (checkSorted(db->ptrA, 0, db->n_ptrA - 1, cmpType) == 1) {
    i = 2;
  } else {
    i = MergeSort(db->ptrA, 0, db->n_ptrA - 1, cmpType);
  }

  if (i == 0) {
    printf("Pole bylo serazeno a vysledek je vlozen do pomocneho pole.\n");
  } else if (i == 2) {
    printf("Pole je jiz serazeno.\n");
  }

  return i;
}

/**
 * @brief Callback funkce pro znovunaplneni pomocneho pole, kdyz potrebuji zahajit novy filtr
 * @param db Predana databaze
 *
 * @return 0
*/
int ResetPom(DB* db){

  for (int i = 0; i < db->n; i++) (db->ptrA)[i] = &(db->znamky[i]);

  db->n_ptrA = db->n;
  printf("Pomocne pole bylo vyresetovano a nyni obsahuje opet %d prvku.\n", db->n_ptrA);
  return 0;
}
/**
 * @brief Po zavolani nastavi promennou running uvnitr poskytnute databaze na 0
 *a predejde dalsimu opakovani dotazovani v hlavnim menu
 * @param db Predana databaze
 * @return 0
 **/
int UkonciProgram(DB* db) {
  printf("Koncim Program\n");
  db->running = 0;
  return 0;
}

typedef struct _polozka {
  char nazev[51];
  char klavesa;
  Takce akce;

} Tpolozka;

// Struktura menu, ktere se sklada z nejake aktivaci klavesy, textu a callback
// funkce, kterou spusti po zadani patricne klavesy
const Tpolozka MENU[] = {
    [0] = {.nazev = "Nacti soubor", .klavesa = '1', .akce = NactiSoubor},
    [1] = {.nazev = "Vypis obsah DB", .klavesa = '2', .akce = VypisDB},
    [2] = {.nazev = "Vypis obsah pomocneho pole",
           .klavesa = '3',
           .akce = VypisPom},
    [3] = {.nazev = "Pridej znamku", .klavesa = '4', .akce = VlozZnamku},
    [4] = {.nazev = "Uprav znammku", .klavesa = '5', .akce = UpravZnamku},
    [5] = {.nazev = "Serad znamky", .klavesa = '6', .akce = SeradPole},
    [6] = {.nazev = "Smaz znamku", .klavesa = '7', .akce = SmazZnamku},
    [7] = {.nazev = "Filtruj znamky", .klavesa = '8', .akce = FiltrujZnamky},
    [8] = {.nazev = "Resetuj pomocne pole", .klavesa = '9', .akce = ResetPom},
    [9] = {.nazev = "Ukonci program", .klavesa = '0', .akce = UkonciProgram},
};

void tiskMenu(const Tpolozka MENU[]) {
  int i = 0;
  printf("***************************\n");
  do {
    printf("  %c ... %-50s\n", MENU[i].klavesa, MENU[i].nazev);

  } while (MENU[i++].klavesa != '0');
  printf("***************************\n");
}

/**
 * Funkce vraci spravnou callback funkci, ktere odpovida spousteci klavesa.
 *
 * @param klavesa Spousteci klavesa callback funkce
 * @param MENU Struktura menu, ve ktere ma funkce hledat
 *
 * @return Callback funkce
 */
Takce najdiAkci(char klavesa, const Tpolozka MENU[]) {
  int i = 0;
  do {
    if (MENU[i].klavesa == klavesa) return MENU[i].akce;

  } while (MENU[i++].klavesa != '0');
  return NULL;
}

/**
 * @brief Funkce pro inicializace databaze
 * @param db databaze, kterou chci inicializovat
 */
int init_DB(DB* db){
  db->json = (cJSON*)NULL;
  db->nextID = 1;
  db->n = 0;
  db->n_max = 0;
  db->n_ptrA = 0;
  db->n_ptrA_max = 0;

  db->n_pololeti = 0;
  db->n_predmety = 0;
  db->n_predmetyZkr = 0;
  db->n_skolniRoky = 0;
  db->n_ucitele = 0;
  db->n_uciteleZkr = 0;

  db->znamky = (ZNAMKA*)malloc(DEFAULT_MAX * sizeof(ZNAMKA));
  db->n_max = DEFAULT_MAX;
  if (db->znamky == NULL) {
    fprintf(stderr, "Nepodarilo se vytvorit misto v pameti.\n");
    return -1;
  }

  db->ptrA = (ZNAMKA**)malloc(DEFAULT_MAX * sizeof(ZNAMKA*));
  db->n_ptrA_max = DEFAULT_MAX;
  if (db->ptrA == NULL) {
    fprintf(stderr, "Nepodarilo se vytvorit misto v pameti.\n");
    return -1;
  }
  return 0;

}

/**
 * @brief Funkce pro uvolneni vsech veci v databazi
 * @param db databaze, kterou chci uvolnit
 */
void free_DB(DB* db){

  for (int i = 0; i < db->n; i++) {
    free(db->znamky[i].pololeti);
    free(db->znamky[i].popis);
    free(db->znamky[i].predmet);
    free(db->znamky[i].predmetZkr);
    free(db->znamky[i].skolniRok);
    free(db->znamky[i].ucitel);
    free(db->znamky[i].ucitelZkr);
  }
  for (int i = 0; i < db->n_predmetyZkr; i++) free(db->predmetyZkr[i]);
  for (int i = 0; i < db->n_ucitele; i++) free(db->ucitele[i]);
  for (int i = 0; i < db->n_uciteleZkr; i++) free(db->uciteleZkr[i]);
  for (int i = 0; i < db->n_pololeti; i++) free(db->pololeti[i]);
  for (int i = 0; i < db->n_skolniRoky; i++) free(db->skolniRoky[i]);
  for (int i = 0; i < db->n_predmety; i++) free(db->predmety[i]);

  free(db->znamky);
  free(db->ptrA);

  if (db->json != (cJSON*)NULL) cJSON_Delete(db->json);
}

int main(void) {
  DB db;
  db.running = 1;

  if (init_DB(&db) != 0) {
    printf("Nepodarilo se inicializovat databazi.\n");
    return -1;
  }

  char zn;
  int id = -1;

  printf("\n%s\n%55s\n\n", LOGO, "Martin Zezula 4. F");

  do {
    tiskMenu(MENU);

    printf("\nZadejte akci nebo ID znamky\n>> ");

    // Nacteni celeho prikazu, pro funkci se rozhoduju podle prvniho znaku
    scanf(" %20[^\n]", db.prikaz);
    zn = db.prikaz[0];

    printf("\n");

    // Pokud je prvni znak 'i', uzivatel chce vypsat info o znamce s urcitym ID
    if (zn == 'i') {
      sscanf(&(db.prikaz[1]), "%d", &id);
      if (id < 1) {
        printf("Spatny format ID.\n\n");
        continue;
      }
      if (id >= db.nextID) {
        printf("Prvek s timto ID neexistuje.\n\n");
        continue;
      }

      int idx = BinarySearch(db.znamky, db.n, id);

      if (idx == -1) {
        printf("Prvek s timto ID neexistuje.\n\n");
        continue;
      }

      VypisZnamku(db.znamky[idx], 1);

    } else {
      id = -1;
      Takce zavolam = najdiAkci(zn, MENU);
      if (zavolam != NULL) zavolam(&db);
      else{
        printf("Tato moznost neni v nabidce.\nZadejte cislo moznosti nebo ID existujici znamky ve formatu \"i__\".\n");
      }
    }

    printf("\n");

    fflush(stdin);

  } while (db.running == 1);

  free_DB(&db);

  return 0;
}
